# static-tendril miscweb static websites
Blubber images for static-tendril static html sites.
https://tendril.wikimedia.org and https://dbtree.wikimedia.org

## Local development
Build image locally:

```
DOCKER_BUILDKIT=1 docker build --target statictendril -f .pipeline/blubber.yaml .
```

Run image:

```
docker run -p 8080:8080 <image name>
```
Visit `http://127.0.0.1:8080/`

## Publish new image version

To create a new image version merge your change into the main branch. 
This triggers the publish-image pipeline. Image is available at `docker-registry.wikimedia.org/repos/sre/miscweb/statictendril:<timestamp>`
